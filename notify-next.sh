#!/bin/bash

set -e

MSG=$(cat | jq .DepartureTime[1])
notify-send -u low -t 2000 ${MSG:1:-1}
