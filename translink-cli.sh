#!/bin/bash

set -e

ENDPOINT=https://www.translink.co.uk/JourneyPlannerApi/GetJourneyResults
CURL=$(which curl)
CURL_OPS='-H "Content-Type: application/json" -d' # make sure -d is the last option.
DATETIME=`date '+%Y-%m-%d %H:%M'`
PAYLOAD='{"OriginId":"10011918%3A%24Z0","DestinationId":"suburbID%3A31400030%3A147%3ABelfast%3A733748%3A125917%3AITMR","DepartureDate":"'$DATETIME'","FindBusDepartures":true,"FindTrainDepartures":true}'

# Print out the variable status if debug is passed as an argument.
if [[ $1 == "debug" ]]; then
	echo -e "[=] ENDPOINT\t" $ENDPOINT
	echo -e "[=] CURL\t" $CURL
	echo -e "[=] CURL_OPS\t" $CURL_OPS
	echo -e "[=] DATETIME\t" $DATETIME
	echo -e "[=] PAYLOAD"; echo $PAYLOAD | jq .;
	echo -e "[=] CMD\t"$CMD
	echo -e "\n"
elif [[ -z $1 ]]; then
	CURL_OPS="--silent ${CURL_OPS}"
fi

# Build the command.
CMD="${CURL} ${CURL_OPS} '${PAYLOAD}' ${ENDPOINT}"

# Execute the command, then parse the result.
eval $CMD | jq '{DepartureTime: [.Result.TripResults[].DepartureTime], ArrivalTime: [.Result.TripResults[].ArrivalTime]}'