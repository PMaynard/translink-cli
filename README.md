# TransLink CLI (Belfast)

A small bash script which queries translink for the departure and arrival times from Belfast science park to city centre.

- Translink-CLI: Queries the translink API
- Notify-Next: Takes the output of Translink-CLI and displays it as a system notification.

# Prerequisites

- curl
- jq
- bash

# Install

Make sure `~/bin` is included in your `$PATH`.

```shell
ln -s translink-cli.sh ~/bin/translink-cli
ln -s notify-next.sh ~/bin/notify-next
```

# Usage

Translink-CLI

```shell
>: translink-cli
{
  "DepartureTime": [
    "11:42",
    "11:57",
    "12:12",
    "12:27"
  ],
  "ArrivalTime": [
    "11:53",
    "12:08",
    "12:23",
    "12:38"
  ]
}
```

Notify-Next:

```shell
>: translink-cli | notify-next
```

![screen shot of notification](notify.png)

# Technical Details
## Query Translink

```shell
curl -H "Content-Type: application/json" \
	-d '{"OriginId":"10011918%3A%24Z0","DestinationId":"suburbID%3A31400030%3A147%3ABelfast%3A733748%3A125917%3AITMR","DepartureDate":"2019-10-09 09:08","FindBusDepartures":true,"FindTrainDepartures":true}' \
	https://www.translink.co.uk/JourneyPlannerApi/GetJourneyResults
```

## Parse Response

Parse the Departure and Arrival Locations:

```shell
jq '{DepartLocation: .Result.DepartureLocation.Name, ArrivalLocation: .Result.ArrivalLocation.Name}' test/data/one.json
````

```json
{
	"DepartLocation": "Titanic Quarter, Catalyst Inc",
	"ArrivalLocation": "Belfast, May Street [City Hall]"
}
```

Parse the Duration, Departure, and Arrival Times:

```shell
jq '{DepartureTime: [.Result.TripResults[].DepartureTime], ArrivalTime: [.Result.TripResults[].ArrivalTime], Duration: [.Result.TripResults[].TotalDuration]}' test/data/one.json
````

```json
{
  "DepartureTime": [
    "09:00",
    "09:10",
    "09:20",
    "09:30"
  ],
  "ArrivalTime": [
    "09:11",
    "09:21",
    "09:31",
    "09:41"
  ],
  "Duration": [
    660,
    660,
    660,
    660
  ]
}
```

# Todo

- Query and parse the stops endpoints to allow for more locations.
- Result.TripResults[].Legs[]
	- Stop Location (e.g. a map service or what 3 words)
	- .Fares[]
	- .Alerts[]
	- .PlannedDepartureTime
	- .ActualDepartureTime
	- .PlannedArrivalTime
	- .ActualArrivalTime

# License

Copyright 2019 Peter Maynard

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
